#!/bin/bash
APP_HOME=$HOME/.local/share/rpi-camera-display
mkdir -p $APP_HOME
chmod +x cam.sh
ln -sf $(pwd)/cam.sh $APP_HOME
sudo ln -sf $(pwd)/rpi-camera-display.service /etc/systemd/system/rpi-camera-display.service
sudo systemctl daemon-reload
sudo systemctl enable rpi-camera-display.service

# disable services that are not needed to speed up the boot time
sudo systemctl disable dhcpcd.service
sudo systemctl disable networking.service
sudo systemctl disable ssh.service
sudo systemctl disable ntp.service
sudo systemctl disable dphys-swapfile.service
sudo systemctl disable keyboard-setup.service
sudo systemctl disable apt-daily.service
sudo systemctl disable wifi-country.service
sudo systemctl disable hciuart.service
sudo systemctl disable raspi-config.service
sudo systemctl disable avahi-daemon.service
sudo systemctl disable triggerhappy.service
sudo systemctl disable man-db.service

echo '
hdmi_force_hotplug=1
hdmi_group=2
hdmi_mode=82
enable_uart=1
disable_splash=1
dtparam=audio=off
dtoverlay=disable-bt
dtoverlay=disable-wifi
boot_delay=0
disable_poe_fan=1
' | sudo tee -a /boot/config.txt

cmdline="$(cat /boot/cmdline.txt) logo.nologo quiet plymouth.enable=0"
echo "$cmdline" | sudo tee /boot/cmdline.txt

sudo reboot
