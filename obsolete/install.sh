#!/bin/bash
AUTO_HOME=$HOME/.config/autostart
mkdir -p $AUTO_HOME
ln -sf $(pwd)/rpi-camera-display.desktop $AUTO_HOME
APP_HOME=$HOME/.local/share/rpi-camera-display
mkdir -p $APP_HOME
chmod +x cam.sh
ln -sf $(pwd)/cam.sh $APP_HOME
