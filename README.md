# Rasbperry Pi camera display

Simple project to show the camera to an HDMI display in real time, nothing else. 

## Support

This supports all Raspberry Pi's models. I'm currently using an old Raspberry Pi Zero W (the first
one). It only has 1 CPU core; it's very limited, but it can run the camera at FullHD at 30 fps,
without using any CPU, because the image goes directly from the ISP to the GPU, without bothering
the CPU at all. 

## Requirements

- Install the lite image with the username `pi`, unless you want to change it in the files in this
  repo. I left the default password.

## Installation

I want to minimize the boot time as much as possible. With the stock image is 1 minute. The
installation takes care of removing unused services to cut this time in half. 

1. Get the base image up and running. 
2. Install git and vim (optional).
2. Clone this repository.
3. Execute `bash install.sh`. **Warning!** The Pi will reboot once finishes.

## Debugging

Once the HDMI display is running, there is no space for a terminal; also, once deployed, I don't
have access to a keyboard to modify the camera parameters. 

The solution is running a TTY over Serial. I've used this guide from 
[Jeff Geerling](https://www.jeffgeerling.com/blog/2021/attaching-raspberry-pis-serial-console-uart-debugging). 
Just to sumarize:

1. Connect the UART to USB before booting the Pi.
2. Execute this script on the host computer: 

    `sudo screen /dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A50285BI-if00-port0 115200`

  Your serial device name may be different. Use the 115200 baut rate. 

3. Boot the Pi and wait.
4. If nothing appears, press Ctrl+D on the terminal and a login window should appear. 
5. Enter the credentials and you are free to mess around. 

## Modifying properties

By default, the installation script links the `cam.sh` and `rpi-camera-display.service` files in the
appropiate directory. So you can mess around without worring to copy the files again. You may need 
to restart the Pi. 

To change camera parameters, log in, modify `cam.sh` and then restart the servie: 
`sudo systemctl restart rpi-camera-display.service`. 


## Boot time

The Raspberry Pi Zero with the lite OS needs 1 minute to boot; with the normal OS, needs 1:45
minutes. The Raspberry Pi 2 B+, with the normal OS needs 30 seconds. 

The Raspberry Pi Zero with the lite OS modified needs 30 seconds to boot. 

## Obsolete

In this folder there are some ideas:

### Use this with a current Xorg session

The `old-install.sh` script installs the app to be used in an existing Xorg plus
lightdm environment. If you've installed the normal OS image, use the old install script instead.

**Warning!** This is not mantained. Read the script to understand it and modify it accordingly.

### Use this starting an individual Xorg session

If you enable auto login, and `exec startx` on the `.profile`, this will invoque the `xinitrc` and
start the application. This is slower!

**Warning!** This is not mantained. Read the script to understand it and modify it accordingly.


