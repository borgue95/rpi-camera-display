#!/bin/bash
# analoggain 5
# digitalgain 1.2
# brightness 54
# contrast 2
# ISO 200
raspivid \
    --timeout 0 \
    --raw-format rgb \
    --width 1920 \
    --height 1080 \
    --fullscreen \
    --framerate 24 \
    --exposure off \
    --shutter 40000 \
    --analoggain 3.0 \
    --digitalgain 1.0 \
    --contrast 1 \
    --sharpness 10 \
    --brightness 56 \
    --saturation -10 \
    --awb tungsten \
    --drc off

